# Talk on Liquid Crystals

held by Christian-Roman Gerhorst in ocassion of the

# Seminar on Softmatter and Biophysics 2013 / 2014

## advisors
* Prof. Dr. J. Kierfeld
* Prof. Dr. M. Tolan

## Required programs (tested versions):
* TeXLive 2013
* Python 3.3.3-1
* Perl 5.18.1-1

PDFs in directory `final_versions` (handout- and presentation-version).
