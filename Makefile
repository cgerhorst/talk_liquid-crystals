MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDEXPANSION:

TEXENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(shell pwd)/header-components/:" max_print_line=1048576

CONTENTS := cover \
			history \
			properties \
			categorization \
			phases \
			order_parameter \
			onsager \
			landau \
			applications \
			summary \
			appendix
CONTENTS := $(addprefix talk-contents/, $(addsuffix .tex, $(CONTENTS)))

CROP := structure \
		PAA nem_vec lyotropic_phases main_chain-polymers side_chain-polymers copolymer pn \
		free_energ an_distrib \
		thermometer liq_ferro thermo_pitch infrared_hand lcd
CROP := $(addprefix build/graphic/, $(addsuffix .pdf, $(CROP)))

GRAPHIC := $(addsuffix .jpg, cholesteryl-benzoate-m cholesteryl-benzoate-k mbba lcd_1 deGennes) \
		   $(addsuffix .jpg, lcphase birefringence schlieren-texture triphenylene sodium_stereate dnastructure) TMV.png \
		   $(addsuffix .jpg, phase_isotropic nematic_nodirector smectics chiral_disks bluephase1 bluephase_stack discotic cubic) \
		   unbi.pdf \
		   lc_cell_on-off.jpg \
		   laser.jpg \
		   $(addsuffix .jpg, bp_a meet_room pdlc-working_principle) benzene-orbitals.png
GRAPHIC := $(addprefix graphic/, $(GRAPHIC))

all: build/main_presentation.pdf build/main_handout.pdf

build/main_presentation.pdf: $(addsuffix .tex, $(addprefix header/, talk_header talk_packages) main) literature.bib $(CONTENTS) $(GRAPHIC) $(CROP)| build
	@./tex.sh main.tex main_presentation

build/main_handout.pdf: $(addsuffix .tex, $(addprefix header/, talk_header talk_packages) main) literature.bib $(CONTENTS) $(GRAPHIC) $(CROP)| build
	@./tex.sh main.tex main_handout

build/graphic/%.pdf: graphic/%.crop | build/graphic
	@./crop.py graphic/$*

build build/graphic:
	mkdir -p build/graphic

fast:
	@./tex.sh --fast main.tex main_presentation

clean:
	rm -rf build

.PHONY: clean fast
